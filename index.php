<!DOCTYPE html>
<html lang="en">
    <head>
    <title>Emagia</title>
    <link rel="stylesheet" href="resources\css\bootstrap.min.css" />
    
    <script>
    gameStateInfo = '-- New Game --\n';
    
    function playGame() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var myArr = JSON.parse(this.responseText);
                
                myArr.forEach(outputGameStateData);
                gameStateInfo = '-- New Game --\n';
            }
        };
        xhttp.open("GET", "Controllers/index.php", true);
        xhttp.send();
    }
    
    function initialState(param) {
        document.getElementById("player-name").setAttribute('value', param['player']['name']);
        document.getElementById("player-health").setAttribute('value', param['player']['health']);
        document.getElementById("player-defence").setAttribute('value', param['player']['defence']);
        document.getElementById("player-strength").setAttribute('value', param['player']['strength']);
        document.getElementById("player-speed").setAttribute('value', param['player']['speed']);
        document.getElementById("player-luck").setAttribute('value', param['player']['luck']);
        
        document.getElementById("beast-name").setAttribute('value', param['beast']['name']);
        document.getElementById("beast-health").setAttribute('value', param['beast']['health']);
        document.getElementById("beast-defence").setAttribute('value', param['beast']['defence']);
        document.getElementById("beast-strength").setAttribute('value', param['beast']['strength']);
        document.getElementById("beast-speed").setAttribute('value', param['beast']['speed']);
        document.getElementById("beast-luck").setAttribute('value', param['beast']['luck']);
    }
    
    function outputGameStateData(state, index) {
        if(index == 0) {
            initialState(state);
            gameStateInfo += state['action'] + '\n';
        } else {
            state['action'].forEach(outputAllActions);
        }
        document.getElementById("game-states").value = gameStateInfo;
    }
    
    function outputAllActions(action, index) {
        gameStateInfo += action[0] + '\n' + action[1] + '\n';
    }
    
    </script>
    </head>
    <body>
    <div class="container-fluid">
        <main class="col-md-12" role="main">
            <h1 class="bd-title" id="content">Emagia</h1>
            <button type="button" class="btn btn-primary" onClick="playGame()">Start Game</button>
            <div class="row">
                <div class="col-md-6">
                    <div id="player-info">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Player:</span>
                            </div>
                            <input id="player-name" type="text" class="form-control" readonly/>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Health:</span>
                            </div>
                            <input id="player-health" type="text" class="form-control" readonly/>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Defence:</span>
                            </div>
                            <input id="player-defence" type="text" class="form-control" readonly/>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Strength:</span>
                            </div>
                            <input id="player-strength" type="text" class="form-control" readonly/>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Speed:</span>
                            </div>
                            <input id="player-speed" type="text" class="form-control" readonly/>
                    </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Luck:</span>
                            </div>
                            <input id="player-luck" type="text" class="form-control" readonly/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="beast-info">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Enemy:</span>
                            </div>
                            <input id="beast-name" type="text" class="form-control" readonly/>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Health:</span>
                            </div>
                            <input id="beast-health" type="text" class="form-control" readonly/>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Defence:</span>
                            </div>
                            <input id="beast-defence" type="text" class="form-control" readonly/>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Strength:</span>
                            </div>
                            <input id="beast-strength" type="text" class="form-control" readonly/>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Speed:</span>
                            </div>
                            <input id="beast-speed" type="text" class="form-control" readonly/>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Luck:</span>
                            </div>
                            <input id="beast-luck" type="text" class="form-control" readonly/>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Game Log:</span>
                    </div>
                    <textarea class="form-control" id="game-states" rows="20" readonly></textarea>
                </div>
            </div>
        </main>
    </div>
    <script type="text/javascript" src="resources\js\bootstrap.min.js" />
    </body>
</html>


<?php