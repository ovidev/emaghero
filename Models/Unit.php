<?php

class Unit
{
    protected const DEFAULT_ATTACK_COUNTER_MODIFIER = 0;
    protected const DEFAULT_DEFENCE_MODIFIER = 1;
    
    protected $name;
    protected $health;
    protected $strength;
    protected $defence;
    protected $speed;
    protected $luck;
    public $attackCounterModifier;
    public $defenceModifier;
    
    
    function __construct(){
        $this->setDefaultModifiers();
    }
    
    public function getUnitFormat()
    {
        $rawData = [
            'name' => $this->name,
            'health' => $this->health,
            'strength' => $this->strength,
            'defence' => $this->defence,
            'speed' => $this->speed,
            'luck' => $this->luck,
        ];
        return $rawData;
    }
    
    public function setDefaultModifiers(){
        $this->attackCounterModifier = self::DEFAULT_ATTACK_COUNTER_MODIFIER;
        $this->defenceModifier = self::DEFAULT_DEFENCE_MODIFIER;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function setName($newValue){
        $this->name = $newValue;
    }
    
    public function getHealth(){
        return $this->health;
    }
    
    public function setHealth($newValue){
        $this->health = $newValue;
    }
    
    public function getStrength(){
        return $this->strength;
    }
    
    public function setStrength($newValue){
        $this->strength = $newValue;
    }
    
    public function getDefence(){
        return $this->defence;
    }
    
    public function setDefence($newValue){
        $this->defence = $newValue;
    }
    
    public function getSpeed(){
        return $this->speed;
    }
    
    public function setSpeed($newValue){
        $this->speed = $newValue;
    }
    
    public function getLuck(){
        return $this->luck;
    }
    
    public function setLuck($newValue){
        $this->luck = $newValue;
    }
    
    public function roleLuck(){
        return rand(1, 100) <= $this->luck;
    }
    
    public function useSkills(){}
    
    public function defend($enamyAttack){
        if($enamyAttack == 0){
            return;
        }
        $damage = ($enamyAttack - $this->defence) * $this->defenceModifier;
        if($this->health - $damage  <= 0) {
            $this->setHealth(0);
            return;
        }
        $this->setHealth($this->health - $damage);
    }
}
