<?php

require_once('Unit.php');

class Beast extends Unit
{
    protected const MIN_HEALTH = 60;
    protected const MAX_HEALTH = 90;
    protected const MIN_STRENGTH = 60;
    protected const MAX_STRENGTH = 90;
    protected const MIN_DEFENCE = 40;
    protected const MAX_DEFENCE = 60;
    protected const MIN_SPEED = 40;
    protected const MAX_SPEED = 60;
    protected const MIN_LUCK = 25;
    protected const MAX_LUCK = 40;
    
    function __construct(){
        parent::__construct();
        $this->setName('Beast');
        $this->setHealth(rand(self::MIN_HEALTH, self::MAX_HEALTH));
        $this->setStrength(rand(self::MIN_STRENGTH, self::MAX_STRENGTH));
        $this->setDefence(rand(self::MIN_DEFENCE, self::MAX_DEFENCE));
        $this->setSpeed(rand(self::MIN_SPEED, self::MAX_SPEED));
        $this->setLuck(rand(self::MIN_LUCK, self::MAX_LUCK));
    }
}
