<?php

require_once('Unit.php');

class Orderus extends Unit
{
    protected const MIN_HEALTH = 70;
    protected const MAX_HEALTH = 100;
    protected const MIN_STRENGTH = 70;
    protected const MAX_STRENGTH = 80;
    protected const MIN_DEFENCE = 45;
    protected const MAX_DEFENCE = 55;
    protected const MIN_SPEED = 40;
    protected const MAX_SPEED = 50;
    protected const MIN_LUCK = 10;
    protected const MAX_LUCK = 30;
    
    protected $rapidStrikeChance = 10;
    protected $magicShieldChance = 20;
    
    function __construct(){
        parent::__construct();
        $this->setName('Orderus');
        $this->setHealth(rand(self::MIN_HEALTH, self::MAX_HEALTH));
        $this->setStrength(rand(self::MIN_STRENGTH, self::MAX_STRENGTH));
        $this->setDefence(rand(self::MIN_DEFENCE, self::MAX_DEFENCE));
        $this->setSpeed(rand(self::MIN_SPEED, self::MAX_SPEED));
        $this->setLuck(rand(self::MIN_LUCK, self::MAX_LUCK));
    }
    
    public function useSkills(){
        $this->skillRapidStrike();
        $this->skillMagicShield();
    }
    
    public function skillRapidStrike(){
        if(rand(1, 100) <= $this->rapidStrikeChance){
            $this->attackCounterModifier = 1;
        }
    }
    
    public function skillMagicShield(){
        if(rand(1, 100) <= $this->magicShieldChance){
            $this->defenceModifier = 0.5;
        }
    }
}
