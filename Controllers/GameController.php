<?php

require_once('..\Models\Orderus.php');
require_once('..\Models\Beast.php');

class GameController
{
    protected const MAX_TURNS = 20;
    protected $currentTurn = 0;
    protected $turnOrder;
    protected $gameLog;
    
    protected $player;
    protected $beast;
    protected $status;
    
    function __construct(){
        $this->player = new Orderus();
        $this->beast = new Beast();
        $this->setTurnOrder();
        $this->status = 1;
    }
    
    protected function setTurnOrder(){
        if($this->player->getSpeed() > $this->beast->getSpeed()){
            $this->setTurnOrder = 1;
            return;
        }
        if($this->player->getSpeed() == $this->beast->getSpeed() && $this->player->getLuck() > $this->beast->getLuck()){
            $this->setTurnOrder = 1;
            return;
        }
        $this->setTurnOrder = 2;
    }
    
    protected function turn(){
        $this->currentTurn++;
        $turnMessage = [];
        
        switch($this->setTurnOrder){
            case 1:
                for($attackCount = 0; $attackCount <= $this->player->attackCounterModifier; $attackCount++){
                    $bHP = $this->beast->getHealth();
                    $turnMessage[] = $this->makePhaseMessage($this->phase(1), $this->beast->getHealth(), $bHP, 'Beast');
                    if($this->status == 0){
                        return $turnMessage;
                    }
                }
                
                for($attackCount = 0; $attackCount <= $this->beast->attackCounterModifier; $attackCount++){
                    $pHP = $this->player->getHealth();
                    $turnMessage[] = $this->makePhaseMessage($this->phase(2), $this->player->getHealth(), $pHP, 'Orderus');
                    if($this->status == 0){
                        return $turnMessage;
                    }
                }
                break;
            case 2:
                for($attackCount = 0; $attackCount <= $this->beast->attackCounterModifier; $attackCount++){
                    $pHP = $this->player->getHealth();
                    $turnMessage[] = $this->makePhaseMessage($this->phase(2), $this->player->getHealth(), $pHP, 'Orderus');
                    if($this->status == 0){
                        return $turnMessage;
                    }
                }
                
                for($attackCount = 0; $attackCount <= $this->player->attackCounterModifier; $attackCount++){
                    $bHP = $this->beast->getHealth();
                    $turnMessage[] = $this->makePhaseMessage($this->phase(1), $this->beast->getHealth(), $bHP, 'Beast');
                    if($this->status == 0){
                        return $turnMessage;
                    }
                }
                break;
        }
        return $turnMessage;
    }
    
    protected function makePhaseMessage($message, $health, $originalHealth, $unitName){
        $damage = $originalHealth - $health;
        if($health == 0) {
            $this->status = 0;
            return [$message, $unitName . ' got ' . $damage . ' points of Damage! Health at ' . $health  . '. ' . $unitName . ' is Dead!'];
        }
        return [$message, $unitName . ' got ' . $damage . ' points of Damage! Health at ' . $health . '.'];
    }
    
    protected function phase($order){
        switch($order){
            case 1:
                $luckRole = $this->beast->roleLuck();
                if ($this->beast->roleLuck()) {
                    return 'Miss!';
                }
                $this->beast->defend($this->player->getStrength());
                return 'Hit!';
                break;
            case 2:
                $luckRole = $this->player->roleLuck();
                if ($this->player->roleLuck()) {
                    return 'Miss!';
                }
                $this->player->defend($this->beast->getStrength());
                return 'Hit!';
                break;
        }
    }
    
    public function gameStart(){
        $this->gameLog[$this->currentTurn] = [
            'player' => $this->player->getUnitFormat(),
            'beast' => $this->beast->getUnitFormat(),
            'status' => $this->status,
            'order' => ($this->setTurnOrder == 1)? 'Orderus': 'Beast',
            'action' => 'Orderus endered the ever-green forests of Emagia!'
        ];
        
        while($this->currentTurn < self::MAX_TURNS && $this->status) {
            $this->player->useSkills();
            $this->beast->useSkills();
            $turn = $this->turn();
            $this->player->setDefaultModifiers();
            $this->beast->setDefaultModifiers();
            $this->gameLog[$this->currentTurn] = [
                'player' => $this->player->getUnitFormat(),
                'beast' => $this->beast->getUnitFormat(),
                'status' => $this->status,
                'order' => ($this->setTurnOrder == 1)? 'Orderus': 'Beast',
                'action' => $turn
            ];
        }
        
        return json_encode($this->gameLog);
    }
}
